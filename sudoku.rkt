#lang racket

;;
;; Programminging Language Paradigms (UG) Coursework 2015/2016
;; Sudoku Assignment
;; Submitted by: Pete Whelpton (pwhelp01 / 12828513)
;; Lecturers: Keith Mannock / Trevor Fenner
;; Due date: 1st April 2016
;;

;; Changes
;; -------
;; 02/01/2016 - Started on basic functions to create rows
;; 03/01/2016 - Noticed that the spec specifies the row should be a list of SETS, not a list of lists.  Ammended
;; 27/03/2016 - Added methods for getting rows / cols / boxs and checking if the matrix is solved

;; References
;; ----------
;; DeleteNth function not my work!  Written by Matthias Felleisen and taken from the Racket mailing list
;; https://lists.racket-lang.org/users/archive/2012-October/054298.html
;; Transform-row funtion copied pretty much verbatim from the Racket documentation
;; http://docs.racket-lang.org/guide/Lists__Iteration__and_Recursion.html

;; Expose all functions for the test suite (for some reason, DrRacket decided
;; to make this file it's own module when it compiled..)
(provide (all-defined-out))


;;===============================
;; Main Functions
;;===============================

;; Main solving method
;; Transforms the matrix to the list of list of sets format, calls innersolve recursively
;; until a solution is found (or can't be found), then flatten back to a list of lists
(define (solve matrix)
    (flatten-matrix(inner-solve(transform matrix) '()))
  )

;; Iterate over every row in the given matrix, replacing each integer 
;; with a set of integers of the possible values (e.g. a singleton set, or
;; a set of 1 -9)
(define (transform matrix)
  (map (lambda (row)
              (transform-row row))
       matrix
       )
  )

;; Recurse over an unsolved matrix applying method1 and method 2 until it is solved
(define (inner-solve matrix oldmatrix)
  (if (solved? matrix)
      matrix
      (if (equal? matrix oldmatrix)
          matrix
      (inner-solve (method2 (method1 matrix)) matrix))
  ))

;;===============================
;; Helper functions
;;===============================

;; Process matrix - does the bulk of the heavy lifting - iterating through each cell 1-by-1
;; and processing it.  Had to use nested for loops (rather than recursion) because I couldn't
;; think of an (easy) way to know the x/y co-ords of a cell recursively.
(define (method1 matrix)
  (for/list ([row 9])
    (for/list ([col 9])
      (process-cell-m1 matrix row col)))
  )

;; Process matrix - does the bulk of the heavy lifting - iterating through each cell 1-by-1
;; and processing it
(define (method2 matrix)
  (for/list ([row 9])
    (for/list ([col 9])
      (process-cell-m2 matrix row col)))
  )

;; Iterate over every element in a row and replace with a set
;; Based on example on Racket documentation
;; http://docs.racket-lang.org/guide/Lists__Iteration__and_Recursion.html
(define (transform-row row)
  (map (lambda (i) 
         (createset i))
       row
       )
  )

;; Function to create either a list of 0-9 or singleton set
(define (createset i)
  (if (= i 0) (set 1 2 3 4 5 6 7 8 9) (set i))
  )

;; Function to work out if a set is a singleton set
(define (singleton-set? s)
  (= (set-count s) 1)
  )

;; Flatten the matrix for list of list of sets back to a list of lists
(define (flatten-matrix matrix)
  (map (lambda (row) (flatten-row row))
       matrix
       )
  )

;; Flaten the singleton-sets in a row of the matrix back to a list
(define (flatten-row row)
  (map (lambda (s) 
         (flatten-set s))
       row
       )
  )

;; Flatten singleton set to a single integer (if set is not solved, do not flatten)
(define (flatten-set s)
  (if (= (set-count s) 1)
      (list-ref (set->list s) 0)
      s)
  )

;; Work out if the matrix is solved (assume solved if all cells are singlton sets)
(define (solved? matrix)
  (andmap (lambda (row)
              (all-singletons? row))
       matrix
       )
  )

;; Helper function to work out if all sets in a given row are singletons.  Used above.
(define (all-singletons? row)
  (andmap singleton-set? row)
  )


;; Get row / column / box funtions ;;

;; Gets a row from the matrix.  Use to compare a cell to its peers.
;; Is just a wrapper for list-ref, but makes code more 
(define (get-row matrix row)
  (list-ref matrix row))

;; Get a column from the matrix.  Use to compare a cell to its peers.
(define (get-column matrix col)
  (map (lambda (row) (list-ref row col))
       matrix))

;; Get a 3x3 box from the matrix.  Use to compare a cell to its peers.
;; Works on the notion that the quotient of 0/1/2 over 3 will be 0, 3/4/5 over 3 will be 1 etc.
(define (get-box matrix row col)
  ; Work out which box the give cell co-ords are in
  (define box-row(quotient row 3))
  (define box-col(quotient col 3))
  
  ; Make lists of possible values by finding all rows/cols in same 'box' 
  (define row-pos [filter (lambda (i) (= (quotient i 3) box-row)) '(0 1 2 3 4 5 6 7 8 9)])
  (define col-pos [filter (lambda (i) (= (quotient i 3) box-col)) '(0 1 2 3 4 5 6 7 8 9)])
  
  ; Iterate over those values to get the cells, and combine into a new list.  Really annoyed
  ; that I couldn't think of a more 'functional' way of doing this..
  (for*/list ([row row-pos] [col col-pos])
    (list-ref (list-ref matrix row) col))
  )

;; Get an individual cell's contents
(define (get-cell matrix row col)
  (list-ref (list-ref matrix row) col)
  )

;; Create a set of singletons from a list of sets
(define (get-singletons l)
  (foldl (lambda (x r) (set-union x r)) (set) (reduce-to-singletons l))
  )

;; Filter a list so it only contatins singleton sets
(define (reduce-to-singletons l)
  (filter singleton-set? l))

;; Process cell - Method 1.  If cell is a singleton, do nothing (return the cell) as it is solved.
;; If cell contains a non-singleton set, find the complement (i.e. remove) peer singletons
(define (process-cell-m1 matrix row col)
  (let ([cell (get-cell matrix row col)])
    (if (singleton-set? cell)
        cell
        (set-subtract cell (get-peer-singletons matrix row col))))
  )

;; Process cell - method 2.  If cell is a singleton, do nothing (return the cell) as it is solved.
;; If cell contains a non-singleton set, check for a unique value amongst peer rows / cols / boxes
;; by finding the complement and seeing if it is a singleton
(define (process-cell-m2 matrix row col)
  (let ([cell (get-cell matrix row col)])
    (if (singleton-set? cell)
        cell
        (if (singleton-set? (set-subtract cell (get-peer-row matrix row col)))
            (set-subtract cell (get-peer-row matrix row col))

            (if (singleton-set? (set-subtract cell (get-peer-column matrix row col)))
                (set-subtract cell (get-peer-column matrix row col))

                (if (singleton-set? (set-subtract cell (get-peer-box matrix row col)))
                    (set-subtract cell (get-peer-box matrix row col))

                cell
                )
                )
            )
        )
    )
  )

;; Get peer singletons.  Creates a set of singletons from the peers of a given cell
(define (get-peer-singletons matrix row col)
  (set-union(get-singletons (get-row matrix row))
            (get-singletons (get-column matrix col))
            (get-singletons (get-box matrix row col)))
  )


;; Get a 3x3 box from the matrix.  Use to compare a cell to its peers.
;; Works on the notion that the quotient of 0/1/2 over 3 will be 0, 3/4/5 over 3 will be 1 etc.
(define (get-box2 matrix row col)
  ; Work out which box the given cell co-ords are in
  (define box-row(quotient row 3))
  (define box-col(quotient col 3))
  
  ; Make lists of possible values by finding all rows/cols in same 'box' 
  (define row-pos [filter (lambda (i) (= (quotient i 3) box-row)) '(0 1 2 3 4 5 6 7 8 9)])
  (define col-pos [filter (lambda (i) (= (quotient i 3) box-col)) '(0 1 2 3 4 5 6 7 8 9)])
  
  ; Iterate over those values to get the cells, and combine into a new list.  Really annoyed
  ; that I couldn't think of a more 'functional' way of doing this..
  (for*/list ([row row-pos] [col col-pos])
    (list-ref (list-ref matrix row) col))
  )

;; Flatten a list of sets into their superset
(define (get-sets l)
  (foldl (lambda (x r) (set-union x r)) (set) l)
  )

;; All potential values of all peers as one set.  Used in method 2 - compare the peer values to the
;; individual cell.
(define (get-all-peers matrix row col)
  (set-union (get-sets (deleteNth col (get-row matrix row)))
             (get-sets (deleteNth row (get-column matrix col)))
             (get-sets(deleteNth (+ (* (modulo row 3) 3) (modulo col 3)) (get-box matrix row col)))
             )
  )

;; Get peer candidates from a given row
(define (get-peer-row matrix row col)
  (set-union (get-sets (deleteNth col (get-row matrix row)))))

;; Get peer candidates from a given column
(define (get-peer-column matrix row col)
  (set-union (get-sets (deleteNth row (get-column matrix col)))))

;; Get peer candidates from a given box
(define (get-peer-box matrix row col)
  (set-union (get-sets(deleteNth (+ (* (modulo row 3) 3) (modulo col 3)) (get-box matrix row col)))))


;; Delete Nth term from a list
;; ** NOT MY WORK ** written by Matthias Felleisen and taken from the Racket mailing list
;; https://lists.racket-lang.org/users/archive/2012-October/054298.html
(define (deleteNth n l)
  (cond
    [(= n 0) (rest l)]
    [(< n (length l)) (append (take l n) (rest (drop l n)))]
    [else l])
  #;
  (cond
    [(empty? l) l]
    [(zero? n) (rest l)]
    [else (cons (first l) (deleteNth (sub1 n) (rest l)))])
  #;
  (cond
    [(and (zero? n) (empty? l)) l]
    [(and (positive? n) (empty? l)) l]
    [(and (zero? n) (cons? l)) (rest l)]
    [(and (positive? n) (cons? l)) (cons (first l) (deleteNth (sub1 n) (rest l)))]))

