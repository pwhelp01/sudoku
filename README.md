# Programminging Language Paradigms (UG) Coursework 2015/2016 #
## Sudoku Assignment ##
Submitted by: Pete Whelpton (pwhelp01 / 12828513)
Lecturers: Keith Mannock / Trevor Fenner
Due date: 1st April 2016

References
----------
DeleteNth function not my work!  Written by Matthias Felleisen and taken from the Racket mailing list:

https://lists.racket-lang.org/users/archive/2012-October/054298.html

Transform-row funtion copied pretty much verbatim from the Racket documentation:

http://docs.racket-lang.org/guide/Lists__Iteration__and_Recursion.html

A lot of Sudoku language / theory was understood thanks to Peter Novig's essay:
http://norvig.com/sudoku.html

Test Puzzles from Penguin Sudoku 2015